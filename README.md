# O2Sleep(Again)

<center>
<img src="marketing/images/O2Sleep.png" height="200px" />
</center>

## Description

O2 Sleep is a Garmin Connect IQ Application that monitors heart-rate and oxygen saturation, and alerts if the rates fall beneath the minimum thresholds. This can be useful if you already know you have mild or moderate sleep apnea, and want to be notified to change your sleep position to improve your oxygen saturation.

## Disclaimers

Your Garmin device is not a medical device, and this app is not a medical app. How you deal with your sleep apnea is best discussed with a doctor. There are pulse oxymeters available that are certified for medical use and provide this same functionality, with much better accuracy. You should always favour using one of those devices over your Garmin watch for health and safety related needs. Your Garmin watch can be a "better than nothing" tool for when you do not have access to your regular monitoring device.

<center>
<img src="marketing/images/O2SleepAlert.png" height="200px" />
</center>

## Details

Heart-rate and oxygen saturation are gathered from the onboards sensors of a compatible Garmin watch. The API provides the most current data it has available, but note this is almost never actually current for the pulseOx reading. It will show the last calculated reading. How often readings are taken are determined by the pulseOx settings you have specified on your devices, in combination with your stillness. (The pulseOx sensor is only able to capture valid data while you are still).

## What is it good for?

Providing _some_ alerting functionality in situations where you don't have your regular pulse ox monitoring tool. Maybe your oxymeter is broken, or charging, or you are away from home and did not bring it.

If your sleep apnea is mild, you might not choose to wear your regular monitor device in certain situations, like while taking a short nap. Your Garmin watch might be an easy way to provide some sleep improvement in these situations where you were already not using your other monitoring devices.

# Settings

All available settings can be controlled from the Garmin Connect Application, but a subset can also be changed from the device by holding the menu button while on the O2Sleep watch face page.

<center>
<img src="marketing/images/O2SleepUsage.png" height="200px" />
<img src="marketing/images/O2SleepSettings.png" height="200px" />
</center>

| Setting | Default Value | Description |
| ------- | ------------- | ----------- |
| minO2   | 86            | The mininum oxygen saturation reading before an alert is triggered. |
| minHR   | 45            | The mininum heart-rate reading before an alert is triggered. |
| vibrate | 50            | The vibration strength percentage of the alerts. |
| sensorPollSeconds | 4 | The sensor poll frequency. Can be set to short:1/medium:4/long:10 |
| uiRefreshSeconds | 10 | The display update frequency Can be set to short:5/medium:10/long:30. |



## Screenshots

<img src="marketing/images/O2SleepWatch.png" height="400px" />
<img src="marketing/images/SquareWatch.png" height="400px" />
<img src="marketing/images/O2SleepStats.png" height="400px" />
<img src="marketing/images/SquareStats.png" height="400px" />
<img src="marketing/images/O2SleepAlert.png" height="400px" />

## Limitations

- Venu & Venu SQ
  1. The pulseOx sensor data is less reliable than newer models (like the Venu 2)
- All
  1. The puseOx sensors used on wrist-worn wearables are slower and less reliable than available finger-worn pulse oxymeters.
  1. Only alerts based on the last sensor reading. Does not alert if the sensors have not reported recently.
  1. Does not currently store alert history, so you cannot later review when alerts were sent.
  1. The vibration alert disrupts your devices ability to read your pulseOx for a few seconds due to the movement.
  1. Auto closes after a minute if launched from the glance, or from the nightime notification. Launch directly from apps instead.

## License

This code is MIT licensed. See the LICENSE file for the full text.

The NumberFactory.mc file is provided under the Connect IQ SDK license agreement. A copy of the License can be found in source/factories/CIQ-LICENSE-AGREEMENT.html
