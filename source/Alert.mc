import Toybox.WatchUi;
import Toybox.Attention;
import Toybox.Timer;
import Toybox.Lang;
import Toybox.Application.Properties;

class Alert extends WatchUi.View {

    hidden var alertText as String;
    hidden var alertValue as Numeric;
    hidden var showTimer;

    function initialize(text, currentValue as Numeric) {
        View.initialize();

        showTimer = new Timer.Timer();

        alertText = text;
        alertValue = currentValue;

        if (Attention has :vibrate) {
            var vibrate = Properties.getValue("vibrate");
            Attention.vibrate([new Attention.VibeProfile(vibrate, 2000)]);
        }

        if (Attention has :backlight) {
            Attention.backlight(true);
        }
    }

    function onShow() {
        showTimer.start(method(:timeout), 10 * 1000, false);
    }

    function onHide() {
        showTimer.stop();
    }

    function timeout() {
        WatchUi.popView(WatchUi.SLIDE_IMMEDIATE);
    }

    function onUpdate(dc){
        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_TRANSPARENT);

        dc.drawText(dc.getWidth() / 2, dc.getHeight() / 3, Graphics.FONT_LARGE, alertText, Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth() / 2, dc.getHeight() / 1.5, Graphics.FONT_NUMBER_THAI_HOT, alertValue.format("%d"), Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
    }
}