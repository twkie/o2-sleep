import Toybox.Application;
import Toybox.Lang;
import Toybox.WatchUi;
import Toybox.System;

(:background)
class O2SleepApp extends Application.AppBase {
    var monitor;
    var controller;

    function initialize() {
        AppBase.initialize();
    }

    // onStart() is called on application start up
    function onStart(state as Dictionary?) as Void {

    }

    // onStop() is called when your application is exiting
    function onStop(state as Dictionary?) as Void {
        if (monitor != null) {
            monitor.stop();
        }
        if (controller != null) {
            controller.stop();
            controller.removeAlertListeners();
        }
    }

    // Return the initial view of your application here
    function getInitialView() as Array<Views or InputDelegates>? {
        if (monitor == null) {
            monitor = new SleepMonitor();
        }
        if (controller == null) {
            controller = new DisplayController();
        }

        monitor.start();
        controller.start();
        return [ new ClockView(), new ClockDelegate() ] as Array<Views or InputDelegates>;
    }

    public function getServiceDelegate() as Array<ServiceDelegate> {
        Background.registerForSleepEvent();
        Background.registerForTemporalEvent(new Time.Duration(5 * 60));
        if (monitor == null) {
            monitor = new SleepMonitor();
        }
        return [ new BackgroundService() ];
    }

    function onSettingsChanged() {
        monitor.readSettings();
    }

}
