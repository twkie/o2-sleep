import Toybox.Timer;
import Toybox.UserProfile;
import Toybox.Lang;
import Toybox.Time;
import Toybox.Application.Properties;

class DisplayController {

    hidden var mTimer;
    hidden var currentDisplayName;
    hidden var profile;
    hidden var uiRefreshSeconds = 0 as Number;

    function initialize() {
        mTimer = new Timer.Timer();
        profile = UserProfile.getProfile();
        readSettings();
    }

    function readSettings() {
        uiRefreshSeconds = Properties.getValue("uiRefreshSeconds");
    }

    function start() {
        mTimer.start(method(:onTimer), uiRefreshSeconds, true);
        Application.getApp().monitor.addAlertListener("hr", "display", method(:displayHRAlert));
        Application.getApp().monitor.addAlertListener("o2", "display", method(:displayO2Alert));
    }

    function stop() {
        mTimer.stop();
    }

    function removeAlertListeners() {
        Application.getApp().monitor.removeAlertListener("hr", "display");
        Application.getApp().monitor.removeAlertListener("o2", "display");
    }

    function displayHRAlert(current) {
        WatchUi.pushView(
            new Alert("Low HR Alert", current),
            null,
            WatchUi.SLIDE_IMMEDIATE
        );
    }

    function displayO2Alert(current) {
        WatchUi.pushView(
            new Alert("Low O2 Alert", current),
            null,
            WatchUi.SLIDE_IMMEDIATE
        );
    }

    function getFontColor() {
        if (profile.sleepTime != null && profile.wakeTime != null) {
            var currentDuration = Time.now().subtract(Time.today()) as Duration;
            if (profile.sleepTime.lessThan(currentDuration) || profile.wakeTime.greaterThan(currentDuration)) {
                return Graphics.COLOR_DK_GRAY;
            }
        }

        return Graphics.COLOR_WHITE;
    }

    function addDisplay(name) {
        currentDisplayName = name;
        start();
    }

    function removeDisplay(name) {
        if (name.equals(currentDisplayName)) {
            stop();
        }
    }

    function onTimer() {
        if (currentDisplayName != null) {
            WatchUi.requestUpdate();
        }
    }
}