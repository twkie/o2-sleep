import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Timer;

class StatsView extends WatchUi.View {

    function initialize() {
        View.initialize();
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
        Application.getApp().controller.addDisplay("stats");
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        var mMonitor = Application.getApp().monitor;

        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        dc.setColor(Application.getApp().controller.getFontColor(), Graphics.COLOR_TRANSPARENT);

        var fontOffset = (dc.getFontHeight(Graphics.FONT_SMALL) / 2);

        dc.drawText(dc.getWidth() / 2, (dc.getHeight() / 5) - fontOffset, Graphics.FONT_SMALL, "O2", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth() / 2, (dc.getHeight() / 2.5) - fontOffset, Graphics.FONT_NUMBER_THAI_HOT, mMonitor.getCurrentO2(), Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth() / 2, (dc.getHeight() / 1.66) - fontOffset, Graphics.FONT_SMALL, "HR", Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
        dc.drawText(dc.getWidth() / 2, (dc.getHeight() / 1.25) - fontOffset, Graphics.FONT_NUMBER_THAI_HOT, mMonitor.getCurrentHR(), Graphics.TEXT_JUSTIFY_CENTER | Graphics.TEXT_JUSTIFY_VCENTER);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
        Application.getApp().controller.removeDisplay("stats");
    }
}
