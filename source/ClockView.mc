import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Timer;
import Toybox.Time.Gregorian;

class ClockView extends WatchUi.View {

    function initialize() {
        View.initialize();
    }

    // Called when this View is brought to the foreground. Restore
    // the state of this View and prepare it to be shown. This includes
    // loading resources into memory.
    function onShow() as Void {
        Application.getApp().controller.addDisplay("clock");
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {

        dc.setColor(Graphics.COLOR_BLACK, Graphics.COLOR_BLACK);
        dc.clear();
        dc.setColor(Application.getApp().controller.getFontColor(), Graphics.COLOR_TRANSPARENT);

        var now = Gregorian.info(Time.now(), Time.FORMAT_MEDIUM);
        var hour = now.hour;

        if( !System.getDeviceSettings().is24Hour ) {
            hour = hour % 12;
            if (hour == 0) {
                hour = 12;
            }
        }

        var timeString = Lang.format("$1$:$2$", [hour.format("%02d"), now.min.format("%02d")]);
        var dateString = Lang.format("$1$, $2$ $3$", [now.day_of_week, now.month, now.day]);
        var medFontHeight = dc.getFontHeight(Graphics.FONT_NUMBER_MEDIUM) - 10;

        dc.drawText(dc.getWidth() / 2, (dc.getHeight() / 2) - medFontHeight, Graphics.FONT_NUMBER_MEDIUM, timeString, Graphics.TEXT_JUSTIFY_CENTER);
        dc.drawText(dc.getWidth() / 2, dc.getHeight() / 2, Graphics.FONT_SMALL, dateString, Graphics.TEXT_JUSTIFY_CENTER);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
        Application.getApp().controller.removeDisplay("clock");
    }
}
